#include "ros/ros.h"
#include "std_msgs/String.h"
#include "std_msgs/Bool.h"
#include "std_msgs/UInt16.h"
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <sstream>
#include <vector>
#include <geometry_msgs/PoseStamped.h>
#include <mavros_msgs/DebugValue.h>


class SubscribeGiinavPublishMavros
{
    public: 
    SubscribeGiinavPublishMavros()
    {
    _pub = _nh.advertise<mavros_msgs::DebugValue>("mavros/debug_value/send",50);
    _sub_pose = _nh.subscribe("GIINAV_POSE", 1, &SubscribeGiinavPublishMavros::poseToMavros, this);
    _sub_status = _nh.subscribe("giinav_status", 10, &SubscribeGiinavPublishMavros::statusToMavros, this);
    }

    void poseToMavros(const geometry_msgs::PoseStamped& msg)
    {
    auto pose = boost::make_shared<mavros_msgs::DebugValue>();
    pose->type = 1; //debug vector
    pose->data.resize(3); 
    pose->name = "Gcoord";
    pose->data[0] = msg.pose.position.x;
    pose->data[1] = msg.pose.position.y;
    pose->data[2] = msg.pose.position.z;
    _pub.publish(pose);
    }

    void statusToMavros(const std_msgs::UInt16& msg)
    {
        auto statusID = boost::make_shared<mavros_msgs::DebugValue>();
        statusID->type = 3; // named_value_float
        statusID->value_float = static_cast<float>(msg.data);
        statusID->name = "Gstatus";
        _pub.publish(statusID);
    }

    private:
    ros::NodeHandle _nh;
    ros::Publisher _pub;
    ros::Subscriber _sub_pose;
    ros::Subscriber _sub_status;
};//End of class SubscribeGiinavPublishMavros

class SubscribeRFPublishMavros
{
    public: 
    SubscribeRFPublishMavros()
    {
    _pub = _nh.advertise<mavros_msgs::DebugValue>("mavros/debug_value/send",1);
    _sub = _nh.subscribe("rf_pose", 1, &SubscribeRFPublishMavros::poseToMavros, this);
    _sub_status = _nh.subscribe("rf_status", 10, &SubscribeRFPublishMavros::statusToMavros, this);
    }

    void poseToMavros(const geometry_msgs::PoseStamped& msg)
    {
    auto pose = boost::make_shared<mavros_msgs::DebugValue>();
    pose->type = 1; //debug vector
    pose->data.resize(3); 
    pose->name = "RFcoord";
    pose->data[0] = msg.pose.position.x;
    pose->data[1] = msg.pose.position.y;
    pose->data[2] = msg.pose.position.z;
    _pub.publish(pose);
    }

    void statusToMavros(const std_msgs::UInt16& msg)
    {
        auto statusID = boost::make_shared<mavros_msgs::DebugValue>();
        statusID->type = 3; // named_value_float
        statusID->value_float = static_cast<float>(msg.data);
        statusID->name = "RFstatus";
        _pub.publish(statusID);
    }

    private:
    ros::NodeHandle _nh;
    ros::Publisher _pub;
    ros::Subscriber _sub;
    ros::Subscriber _sub_status;
};//End of class SubscribeRFPublishMavros

class ToggleGnssOnOff
{
    public:
    ToggleGnssOnOff()
    {
        _sub = _nh.subscribe("mavros/debug_value/named_value_float",1, &ToggleGnssOnOff::detectGnssToggleSignalAndPub, this);
        _pub = _nh.advertise<std_msgs::Bool>("toggleOnOffGnss", 1, true);
    }

    void detectGnssToggleSignalAndPub(const mavros_msgs::DebugValue& msg)
    {
        std::string name = msg.name;
        if (name == "onoffGNSS" && static_cast<int>(msg.value_float) == 1) 
        {
            //turn on
            std_msgs::Bool msg;
            msg.data = true;
            _pub.publish(msg);           
        }
        else if (name == "onoffGNSS" && static_cast<int>(msg.value_float) == 0)
        {
            //turn off
            std_msgs::Bool msg;
            msg.data = false;
            _pub.publish(msg);
        }
        
    }

    private:
    ros::NodeHandle _nh;
    ros::Publisher _pub;
    ros::Subscriber _sub;
};//End of class ToggleGnssOnOff


int main(int argc, char **argv)
{

    ros::init(argc, argv, "ginavToMavros");
    
    SubscribeGiinavPublishMavros SGPMObject; //subscribe to Giinav topics and publish to mavros
    SubscribeRFPublishMavros SRFPMObject; //subscribe to RoamFree topics and publish to mavros
    ToggleGnssOnOff TGOnOffObject; //receive the GNSS toggle signal from QGC and publish on ros topic

    while (ros::ok())
    {
        ros::spinOnce();
    }
    
}
